package com.devcamp.crudatfrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudatfrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudatfrontendApplication.class, args);
	}

}
