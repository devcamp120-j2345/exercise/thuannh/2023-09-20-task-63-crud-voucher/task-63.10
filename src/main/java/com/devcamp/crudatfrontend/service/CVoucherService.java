package com.devcamp.crudatfrontend.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.crudatfrontend.model.CVoucher;
import com.devcamp.crudatfrontend.repository.IVoucherRepository;
@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
}
