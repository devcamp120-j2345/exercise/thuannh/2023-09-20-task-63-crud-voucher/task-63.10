package com.devcamp.crudatfrontend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.crudatfrontend.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
